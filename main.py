"""
Main loop
"""

import os
import sys

import gi
import pandas as pd

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

IMG_DIR = os.path.join(".", "images")

# AVAILABLE_RATINGS = list(range(3))
AVAILABLE_RATINGS = [0, 0.5, 1]

IMAGES = [file for file in os.listdir(IMG_DIR) if ".png" in file]


# example_name = "female-blond-0-07089.npy"


# def parse_data_frame_columns(img_name, score):
#
#     # e.g. 'imgfemale-blond-0-s7089-random.npy'
#     img_name = img_name[3:-4]  # Remove file extension and Ukko2 "img..."
#     # 'female-blond-0-s7089-random'
#     name_split = img_name.split("-")
#     # female blond 0 s7089 random
#     stimulus = name_split[-1]
#     name_split.pop(-1)  # Subject ID, an idetifier preceded by "s"
#     # female blond 0 s7089
#     assert "s" in name_split[-1]
#     subject = name_split[-1][1:]
#     name_split.pop(-1)
#     # female blond 0
#     iteration = name_split[-1]
#     name_split.pop(-1)  # Iteration
#     # female blond
#     combination = f"{len(name_split)} tasks"
#     tasks = "-".join(name_split)
#     return iteration, combination, score, stimulus, tasks, subject


def parse_data_frame_columns(img_name, score):
    """Take image data from name, and score"""

    img_name = img_name.split(".")[0]
    name_split = img_name.split("-")

    event_cats = name_split[0]
    crowd_size = name_split[1]
    # subject_id = name_split[-1]  ####### Useful to go back to individual
    # if subject_id == "collaborative":
    #     design_method = "collaborative"
    # else:
    #     design_method = "individual"
    feedback_model = name_split[-1]
    if "_" in event_cats:
        combined_tasks = len(event_cats.split("_"))
    else:
        combined_tasks = 1
    return {
        "Crowd size": crowd_size,
        # "Subject ID": subject_id,
        "Combined tasks": combined_tasks,
        "Tasks": event_cats,
        "Score": score,
        "Feedback model": feedback_model,
        # "Design method": design_method,
    }


data = pd.DataFrame(
    columns=(
        "Crowd size",
        # "Subject ID",
        "Combined tasks",
        "Tasks",
        "Score",
        "Feedback model",
        # "Design method",
    )
)


class AppWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(
            self,
            title="Facecat User Ratings",
            resizable=False,
            default_width=512,
            default_height=512,
        )
        # ---------------------------------------------------------------------
        self.grid = Gtk.Grid()
        self.add(self.grid)
        # ---------------------------------------------------------------------
        self.image_idx = 0
        self.image = Gtk.Image()
        self.image.set_from_file(os.path.join(IMG_DIR, IMAGES[self.image_idx]))
        self.grid.add(self.image)
        # ---------------------------------------------------------------------
        self.progress_bar = Gtk.ProgressBar()
        self.grid.attach_next_to(
            self.progress_bar, self.image, Gtk.PositionType.BOTTOM, 1, 1
        )
        # ---------------------------------------------------------------------
        self.label = Gtk.Label()
        # _, _, _, _, label_text, _ = parse_data_frame_columns(IMAGES[self.image_idx], 0)
        label_text = parse_data_frame_columns(IMAGES[self.image_idx], 0)["Tasks"]
        self.label_markup = f"<big>{label_text}</big>"
        self.label.set_markup(self.label_markup)
        self.grid.attach_next_to(
            self.label, self.progress_bar, Gtk.PositionType.BOTTOM, 1, 1
        )
        # ---------------------------------------------------------------------
        self.button_box = Gtk.Box(spacing=6)
        buttons_labels = "No match", "Partial match", "Total match"
        # self.buttons = [Gtk.Button(label=f"{rating}") for rating in AVAILABLE_RATINGS]
        self.buttons = [
            Gtk.Button(label=button_label) for button_label in buttons_labels
        ]
        for button in self.buttons:
            button.connect("clicked", self.on_button_clicked)
            self.button_box.pack_start(button, True, True, 0)
        self.grid.attach_next_to(
            self.button_box, self.label, Gtk.PositionType.BOTTOM, 1, 1
        )

    def on_button_clicked(self, widget):
        score_relations = {"No match": 0, "Partial match": 0.5, "Total match": 1}
        selected_score = score_relations[widget.get_label()]
        data.loc[self.image_idx] = parse_data_frame_columns(
            IMAGES[self.image_idx], selected_score
        )
        data.to_csv("data.csv")
        if self.image_idx == len(IMAGES) - 1:
            self.show_finished_dialog()
        self.image_idx += 1
        self.progress_bar.set_fraction(self.image_idx / len(IMAGES))
        self.image.set_from_file(os.path.join(IMG_DIR, IMAGES[self.image_idx]))
        # _, _, _, _, label_text, _ = parse_data_frame_columns(IMAGES[self.image_idx], 0)
        label_text = parse_data_frame_columns(IMAGES[self.image_idx], 0)["Tasks"]
        self.label_markup = f"<big>{label_text}</big>"
        self.label.set_markup(self.label_markup)

    def show_finished_dialog(self):
        text = "Last image rated!"
        secondary_text = "Thanks for all your help :)"
        dialog = Gtk.MessageDialog(
            self,
            buttons=Gtk.ButtonsType.CLOSE,
            text=text,
            secondary_text=secondary_text,
        )
        dialog.run()
        dialog.destroy()
        self.destroy()
        sys.exit()


def main():
    app_window = AppWindow()
    app_window.connect("destroy", Gtk.main_quit)
    app_window.show_all()
    Gtk.main()


if __name__ == "__main__":
    main()
